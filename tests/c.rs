use fie_ffi::c::config::*;

use std::ptr;
use std::ffi;

#[test]
fn test_config() {
    let config = fie_config_new();

    assert!(!config.is_null());

    //Platform setters
    unsafe {
        assert!(fie_config_platform_get(config, ApiType::Twitter));
        fie_config_platform_set(ptr::null_mut(), ApiType::Twitter, false);
        assert!(fie_config_platform_get(config, ApiType::Twitter));
        fie_config_platform_set(config, ApiType::Twitter, false);
        assert!(!fie_config_platform_get(config, ApiType::Twitter));

        assert!(fie_config_platform_get(config, ApiType::Mastodon));
        fie_config_platform_set(config, ApiType::Mastodon, false);
        assert!(!fie_config_platform_get(config, ApiType::Mastodon));

        assert!(fie_config_platform_get(config, ApiType::Minds));
        fie_config_platform_set(config, ApiType::Minds, false);
        assert!(!fie_config_platform_get(config, ApiType::Minds));

        assert!(fie_config_platform_get(config, ApiType::Gab));
        fie_config_platform_set(config, ApiType::Gab, false);
        assert!(!fie_config_platform_get(config, ApiType::Gab));
    }

    //Gab API setters
    unsafe {
        //Get default
        let mut username = ptr::null();
        let mut password = ptr::null();

        assert!(!fie_config_api_gab_get(ptr::null(), &mut username, &mut password));
        assert!(password.is_null());
        assert!(username.is_null());

        assert!(fie_config_api_gab_get(config, &mut username, &mut password));
        assert!(!password.is_null());
        assert!(!username.is_null());

        let expected = ffi::CString::new("").expect("To get CString");
        assert_eq!(expected.as_c_str(), ffi::CStr::from_ptr(username));
        assert_eq!(expected.as_c_str(), ffi::CStr::from_ptr(password));

        fie_string_free(username as *mut _);
        fie_string_free(password as *mut _);

        //Set and get new
        let expected_username = ffi::CString::new("lolka").expect("To get CString");
        let expected_password = ffi::CString::new("qwerty").expect("To get CString");

        assert!(!fie_config_api_gab_set(ptr::null_mut(), expected_username.as_c_str().as_ptr(), expected_password.as_c_str().as_ptr()));
        assert!(fie_config_api_gab_set(config, expected_username.as_c_str().as_ptr(), expected_password.as_c_str().as_ptr()));

        let mut username = ptr::null();
        let mut password = ptr::null();
        assert!(fie_config_api_gab_get(config, &mut username, &mut password));

        assert!(!password.is_null());
        assert!(!username.is_null());
        assert_eq!(expected_username.as_c_str(), ffi::CStr::from_ptr(username));
        assert_eq!(expected_password.as_c_str(), ffi::CStr::from_ptr(password));

        fie_string_free(username as *mut _);
        fie_string_free(password as *mut _);
    }

    unsafe {
        fie_config_free(config);
    }
}
