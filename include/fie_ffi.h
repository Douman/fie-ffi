#ifndef fie_ffi_h
#define fie_ffi_h

/**
 * Automatically generated C bindings to Rust library  https://github.com/DoumanAsh/fie
 *
 * Can be build from https://gitlab.com/Douman/fie-ffi
 */

typedef void API;
typedef void Config;
typedef void Post;

#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

/**
 * Result of send for API
 */
enum ApiResult {
    /**
     * API is not initialized so not sent
     */
    NotSent = 0,
    /**
     * Successfully sent
     */
    Sent,
    /**
     * Error sending
     */
    Error,
};
typedef uint8_t ApiResult;

/**
 * Available social media APIs
 */
enum ApiType {
    /**
     * Twitter
     */
    Twitter = 0,
    /**
     * Gab
     */
    Gab,
    /**
     * Mastodon
     */
    Mastodon,
    /**
     * Minds
     */
    Minds,
};
typedef uint8_t ApiType;

/**
 * Possible type of tokens
 */
enum TokenType {
    /**
     * Consumer
     */
    Consumer = 0,
    /**
     * Access
     */
    Access,
};
typedef uint8_t TokenType;

/**
 * Result of posting.
 */
typedef struct {
    /**
     * Whether was able to send post
     */
    bool result;
    /**
     * Result of twitter post
     */
    ApiResult twitter;
    /**
     * Result of gab post
     */
    ApiResult gab;
    /**
     * Result of mastodon post
     */
    ApiResult mastodon;
    /**
     * Result of minds post
     */
    ApiResult minds;
} PostResult;

/**
 * Specifies APIs to use
 */
typedef struct {
    /**
     * Use twitter.
     */
    bool twitter;
    /**
     * Use gab.
     */
    bool gab;
    /**
     * Use mastodon.
     */
    bool mastodon;
    /**
     * Use minds.
     */
    bool minds;
} UsePlatforms;

/**
 * Takes configuration and creates new API instance.
 * Config is no longer should be used by user
 * Returns NULL if none of APIs could be enabled
 * Consumes `config`
 */
API *fie_api_new(Config *config);

/**
 * Sends Post through API
 * Consumes `post`
 * If sent, returns result. On any failure returns NULL
 */
PostResult fie_api_send_post(API *api, Post *post);

/**
 * Sends Post through API
 * Consumes `post`
 * If sent, returns result.
 * On any failure returns NULL
 * `platforms` is optional argument
 * If specified it determines toward which APIs to send post.
 */
PostResult fie_api_send_post_ext(API *api, Post *post, const UsePlatforms *platforms);

/**
 * Starts Fie's runtime to perform API calls.
 * This function must be called prior to calling any `fie_api_*` function
 * Blocks current thread and returns true.
 * In case of inability to start, returns false.
 */
bool fie_api_start(void);

/**
 * Stops Fie's runtime, does nothing if it hasn't been started prior
 */
void fie_api_stop(void);

/**
 * Gets GAB api configuration.
 * Note that assigned must be freed by `fie_string_free`
 * Returns whether it is been successful.
 */
bool fie_config_api_gab_get(const Config *ptr, const char **access_token);

/**
 * Sets GAB api configuration.
 * Stings MUST be UTF-8
 * Returns whether it is been successfully set
 */
bool fie_config_api_gab_set(Config *ptr, const char *access_token);

/**
 * Gets Mastodon api configuration.
 * Note that assigned must be freed by `fie_string_free`
 * Returns whether it is been successful.
 */
bool fie_config_api_mastodon_get(const Config *ptr, const char **host, const char **access_token);

/**
 * Sets Mastodon api configuration.
 * Stings MUST be UTF-8
 * Returns whether it is been successfully set
 */
bool fie_config_api_mastodon_set(Config *ptr, const char *host, const char *access_token);

/**
 * Gets Minds api configuration.
 * Note that assigned must be freed by `fie_string_free`
 * Returns whether it is been successful.
 */
bool fie_config_api_minds_get(const Config *ptr, const char **username, const char **password);

/**
 * Sets Minds api configuration.
 * Stings MUST be UTF-8
 * Returns whether it is been successfully set
 */
bool fie_config_api_minds_set(Config *ptr, const char *username, const char *password);

/**
 * Gets Twitter api configuration.
 * Note that assigned must be freed by `fie_string_free`
 * Returns whether it is been successful.
 */
bool fie_config_api_twitter_get(const Config *ptr, TokenType typ, const char **key, const char **secret);

/**
 * Sets Twitter api configuration.
 * Stings MUST be UTF-8
 * Returns whether it is been successfully set
 */
bool fie_config_api_twitter_set(Config *ptr, TokenType typ, const char *key, const char *secret);

/**
 * Creates copy of Config, returning pointer with new instance.
 */
Config *fie_config_clone(Config *ptr);

/**
 * Deserializes config from binary
 * Returns NULL on error
 */
Config *fie_config_deserialize(const uint8_t *buffer, uintptr_t size);

/**
 * Frees memory for Config
 */
void fie_config_free(Config *ptr);

/**
 * Creates new default configuration
 */
Config *fie_config_new(void);

/**
 * Gets whether to enable or disable ApiType.
 */
bool fie_config_platform_get(Config *ptr, ApiType api);

/**
 * Sets whether to enable or disable ApiType.
 */
void fie_config_platform_set(Config *ptr, ApiType api, bool value);

/**
 * Serializes config to binary
 * Returns the written size
 * On failure returns to serialize returns 0
 * If out is NULL then returns the expected size
 */
uintptr_t fie_config_serialize(Config *ptr, uint8_t *out, uintptr_t size);

/**
 * Sets timeout value in seconds
 * Default value is 5
 */
void fie_config_setting_timeout(Config *ptr, uint64_t value);

/**
 * Adds image to Post
 * Path must be encoded as UTF-8
 * Returns whether operation is successful.
 * Only up to 4 images are allowed
 */
bool fie_post_add_image(Post *post, const char *path);

/**
 * Appends hashtag to the Post
 * Text must be encoded as UTF-8
 * Returns whether operation is successful
 */
bool fie_post_add_tag(Post *post, const char *text);

/**
 * Sets NSFW flag to the post.
 */
void fie_post_flag_set_snfw(Post *post, bool value);

/**
 * Frees memory for Post
 */
void fie_post_free(Post *ptr);

/**
 * Creates new post with provided text
 * Text must be encoded as UTF-8
 */
Post *fie_post_new(const char *text);

/**
 * Frees memory for String allocated by library
 */
void fie_string_free(char *ptr);

#endif /* fie_ffi_h */
