//!Fie FFI bindings

#![warn(missing_docs)]
#![cfg_attr(feature = "cargo-clippy", allow(clippy::style))]

pub mod common;
pub(crate) mod rt;
#[cfg(feature = "c")]
pub mod c;
