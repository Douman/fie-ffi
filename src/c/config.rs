//!Config FFI bindigns

use fie::config::{Config, Platforms, Settings, ApiConfig};

use core::{slice, ptr};
use std::ffi;
use std::os::raw::{c_char};

#[no_mangle]
///Creates new default configuration
pub extern "C" fn fie_config_new() -> *mut Config {
    let config = Config {
        platforms: Platforms::default(),
        api: ApiConfig::default(),
        settings: Settings::default(),
    };

    Box::into_raw(Box::new(config))
}

#[no_mangle]
///Serializes config to binary
///
///Returns the written size
///On failure returns to serialize returns 0
///If out is NULL then returns the expected size
pub unsafe extern "C" fn fie_config_serialize(ptr: *mut Config, out: *mut u8, size: usize) -> usize {
    match ptr.as_ref() {
        Some(config) => match ptr.is_null() {
            false => {
                let out_size = bincode::serialized_size(config).unwrap_or(0) as usize;
                if out_size > size {
                    return 0
                }
                let out = slice::from_raw_parts_mut(out, size);
                match bincode::serialize_into(out, config) {
                    Ok(_) => out_size,
                    Err(_) => 0
                }
            },
            true => bincode::serialized_size(config).unwrap_or(0) as usize

        },
        None => return 0
    }
}

#[no_mangle]
///Deserializes config from binary
///
///Returns NULL on error
pub unsafe extern "C" fn fie_config_deserialize(buffer: *const u8, size: usize) -> *mut Config {
    if buffer.is_null() {
        return ptr::null_mut();
    }

    let buffer = slice::from_raw_parts(buffer, size);
    match bincode::deserialize_from(buffer) {
        Ok(config) => Box::into_raw(Box::new(config)),
        Err(_) => ptr::null_mut(),
    }
}

#[no_mangle]
///Creates copy of Config, returning pointer with new instance.
pub unsafe extern "C" fn fie_config_clone(ptr: *mut Config) -> *mut Config {
    crate::common::clone_boxed(ptr)
}

#[no_mangle]
///Frees memory for Config
pub unsafe extern "C" fn fie_config_free(ptr: *mut Config) {
    crate::common::free_boxed(ptr);
}

#[repr(u8)]
///Available social media APIs
pub enum ApiType {
    ///Twitter
    Twitter = 0,
    ///Gab
    Gab,
    ///Mastodon
    Mastodon,
    ///Minds
    Minds,
}

#[no_mangle]
///Sets whether to enable or disable ApiType.
pub unsafe extern "C" fn fie_config_platform_set(ptr: *mut Config, api: ApiType, value: bool) {
    if let Some(config) = ptr.as_mut() {
        match api {
            ApiType::Twitter => config.platforms.twitter = value,
            ApiType::Gab => config.platforms.gab = value,
            ApiType::Mastodon => config.platforms.mastodon = value,
            ApiType::Minds => config.platforms.minds = value,
        }
    }
}

#[no_mangle]
///Gets whether to enable or disable ApiType.
pub unsafe extern "C" fn fie_config_platform_get(ptr: *mut Config, api: ApiType) -> bool {
    if let Some(config) = ptr.as_mut() {
        match api {
            ApiType::Twitter => config.platforms.twitter,
            ApiType::Gab => config.platforms.gab,
            ApiType::Mastodon => config.platforms.mastodon,
            ApiType::Minds => config.platforms.minds,
        }
    } else {
        false
    }
}

#[no_mangle]
///Sets timeout value in seconds
///
///Default value is 5
pub unsafe extern "C" fn fie_config_setting_timeout(ptr: *mut Config, value: u64) {
    if let Some(config) = ptr.as_mut() {
        config.settings.timeout = value;
    }
}

#[no_mangle]
///Sets GAB api configuration.
///
///Stings MUST be UTF-8
///
///Returns whether it is been successfully set
pub unsafe extern "C" fn fie_config_api_gab_set(ptr: *mut Config, access_token: *const c_char) -> bool {
    if access_token.is_null() {
        return false;
    }

    let config = match ptr.as_mut() {
        Some(config) => config,
        None => return false
    };

    let access_token = match ffi::CStr::from_ptr(access_token).to_str() {
        Ok(access_token) => access_token,
        Err(_) => return false,
    };

    config.api.gab.access_token.truncate(0);
    config.api.gab.access_token.push_str(access_token);

    true
}

#[no_mangle]
///Gets GAB api configuration.
///
///Note that assigned must be freed by `fie_string_free`
///
///Returns whether it is been successful.
pub unsafe extern "C" fn fie_config_api_gab_get(ptr: *const Config, access_token: *mut *const c_char) -> bool {
    let config = match ptr.as_ref() {
        Some(config) => config,
        None => return false
    };

    match access_token.as_mut() {
        Some(access_token) => {
            let config_access_token = ffi::CString::new(config.api.gab.access_token.as_str()).expect("To get CString");

            *access_token = config_access_token.into_raw();
            true
        }
        _ => false
    }
}

#[no_mangle]
///Sets Minds api configuration.
///
///Stings MUST be UTF-8
///
///Returns whether it is been successfully set
pub unsafe extern "C" fn fie_config_api_minds_set(ptr: *mut Config, username: *const c_char, password: *const c_char) -> bool {
    if username.is_null() || password.is_null() {
        return false;
    }

    let config = match ptr.as_mut() {
        Some(config) => config,
        None => return false
    };

    let username = match ffi::CStr::from_ptr(username).to_str() {
        Ok(username) => username,
        Err(_) => return false,
    };
    let password = match ffi::CStr::from_ptr(password).to_str() {
        Ok(password) => password,
        Err(_) => return false,
    };

    config.api.minds.username.truncate(0);
    config.api.minds.password.truncate(0);

    config.api.minds.username.push_str(username);
    config.api.minds.password.push_str(password);

    true
}

#[no_mangle]
///Gets Minds api configuration.
///
///Note that assigned must be freed by `fie_string_free`
///
///Returns whether it is been successful.
pub unsafe extern "C" fn fie_config_api_minds_get(ptr: *const Config, username: *mut *const c_char, password: *mut *const c_char) -> bool {
    let config = match ptr.as_ref() {
        Some(config) => config,
        None => return false
    };

    match (username.as_mut(), password.as_mut()) {
        (Some(username), Some(password)) => {
            let config_username = ffi::CString::new(config.api.minds.username.as_str()).expect("To get CString");
            let config_passowrd = ffi::CString::new(config.api.minds.password.as_str()).expect("To get CString");

            *username = config_username.into_raw();
            *password = config_passowrd.into_raw();
            true
        }
        _ => false
    }
}

#[no_mangle]
///Sets Mastodon api configuration.
///
///Stings MUST be UTF-8
///
///Returns whether it is been successfully set
pub unsafe extern "C" fn fie_config_api_mastodon_set(ptr: *mut Config, host: *const c_char, access_token: *const c_char) -> bool {
    if host.is_null() || access_token.is_null() {
        return false;
    }

    let config = match ptr.as_mut() {
        Some(config) => config,
        None => return false
    };

    let host = match ffi::CStr::from_ptr(host).to_str() {
        Ok(host) => host,
        Err(_) => return false,
    };
    let access_token = match ffi::CStr::from_ptr(access_token).to_str() {
        Ok(access_token) => access_token,
        Err(_) => return false,
    };

    config.api.mastodon.host.truncate(0);
    config.api.mastodon.access_token.truncate(0);

    config.api.mastodon.host.push_str(host);
    config.api.mastodon.access_token.push_str(access_token);

    true
}

#[no_mangle]
///Gets Mastodon api configuration.
///
///Note that assigned must be freed by `fie_string_free`
///
///Returns whether it is been successful.
pub unsafe extern "C" fn fie_config_api_mastodon_get(ptr: *const Config, host: *mut *const c_char, access_token: *mut *const c_char) -> bool {
    let config = match ptr.as_ref() {
        Some(config) => config,
        None => return false
    };

    match (host.as_mut(), access_token.as_mut()) {
        (Some(host), Some(access_token)) => {
            let config_host = ffi::CString::new(config.api.mastodon.host.as_str()).expect("To get CString");
            let config_access_token = ffi::CString::new(config.api.mastodon.access_token.as_str()).expect("To get CString");

            *host = config_host.into_raw();
            *access_token = config_access_token.into_raw();
            true
        }
        _ => false
    }
}

#[repr(u8)]
///Possible type of tokens
pub enum TokenType {
    ///Consumer
    Consumer = 0,
    ///Access
    Access,
}

#[no_mangle]
///Sets Twitter api configuration.
///
///Stings MUST be UTF-8
///
///Returns whether it is been successfully set
pub unsafe extern "C" fn fie_config_api_twitter_set(ptr: *mut Config, typ: TokenType, key: *const c_char, secret: *const c_char) -> bool {
    if key.is_null() || secret.is_null() {
        return false;
    }

    let config = match ptr.as_mut() {
        Some(config) => config,
        None => return false
    };

    let config = match typ {
        TokenType::Consumer => &mut config.api.twitter.consumer,
        TokenType::Access => &mut config.api.twitter.access,
    };

    let key = match ffi::CStr::from_ptr(key).to_str() {
        Ok(key) => key,
        Err(_) => return false,
    };
    let secret = match ffi::CStr::from_ptr(secret).to_str() {
        Ok(secret) => secret,
        Err(_) => return false,
    };

    config.key.truncate(0);
    config.secret.truncate(0);

    config.key.push_str(key);
    config.secret.push_str(secret);

    true
}

#[no_mangle]
///Gets Twitter api configuration.
///
///Note that assigned must be freed by `fie_string_free`
///
///Returns whether it is been successful.
pub unsafe extern "C" fn fie_config_api_twitter_get(ptr: *const Config, typ: TokenType, key: *mut *const c_char, secret: *mut *const c_char) -> bool {
    let config = match ptr.as_ref() {
        Some(config) => config,
        None => return false
    };

    let config = match typ {
        TokenType::Consumer => &config.api.twitter.consumer,
        TokenType::Access => &config.api.twitter.access,
    };

    match (key.as_mut(), secret.as_mut()) {
        (Some(key), Some(secret)) => {
            let config_key = ffi::CString::new(config.key.as_str()).expect("To get CString");
            let config_secret = ffi::CString::new(config.secret.as_str()).expect("To get CString");

            *key = config_key.into_raw();
            *secret = config_secret.into_raw();
            true
        }
        _ => false
    }
}

#[no_mangle]
///Frees memory for String allocated by library
pub unsafe extern "C" fn fie_string_free(ptr: *mut c_char) {
    if ptr.is_null() {
        return;
    }

    ffi::CString::from_raw(ptr);
}
