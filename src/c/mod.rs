//!C FFI bindings

use core::ptr;
use std::ffi;
use std::os::raw::{c_char};

pub mod config;

use fie::config::Config;
use crate::rt::{self, Runtime, AutoRuntime};

#[no_mangle]
///Starts Fie's runtime to perform API calls.
///
///This function must be called prior to calling any `fie_api_*` function
///
///Blocks current thread and returns true.
///In case of inability to start, returns false.
pub extern "C" fn fie_api_start() -> bool {
    let _guard = match rt::init() {
        Ok(_guard) => _guard,
        Err(_) => return false,
    };

    Runtime::run();

    true
}

#[no_mangle]
///Stops Fie's runtime, does nothing if it hasn't been started prior
pub extern "C" fn fie_api_stop() {
    Runtime::stop();
}

#[no_mangle]
///Takes configuration and creates new API instance.
///
///Config is no longer should be used by user
///
///Returns NULL if none of APIs could be enabled
///
///Consumes `config`
pub extern "C" fn fie_api_new(config: *mut Config) -> *mut fie::API {
    if config.is_null() {
        return ptr::null_mut();
    }

    let config = *unsafe { Box::from_raw(config) };

    let mut any_enabled = false;
    let api = Box::leak(Box::new(fie::API::new(config.settings)));
    let api_ptr = api as *mut _;

    if config.platforms.gab && api.configure_gab(config.api.gab).is_ok() {
        any_enabled = true
    }

    if config.platforms.twitter && api.configure_twitter(config.api.twitter).is_ok() {
        any_enabled = true
    }

    if config.platforms.mastodon && api.configure_mastodon(config.api.mastodon).is_ok() {
        any_enabled = true
    }

    if config.platforms.minds {
        let minds = api.configure_minds(config.api.minds);
        if minds.wait().is_ok() {
            any_enabled = true
        }
    }

    match any_enabled {
        true => api_ptr,
        false => unsafe {
            crate::common::free_boxed(api_ptr);
            ptr::null_mut()
        }
    }
}

#[no_mangle]
///Creates new post with provided text
///
///Text must be encoded as UTF-8
pub unsafe extern "C" fn fie_post_new(text: *const c_char) -> *mut fie::data::Post {
    if text.is_null() {
        return ptr::null_mut();
    }

    let text = match ffi::CStr::from_ptr(text).to_str() {
        Ok(text) => text,
        Err(_) => return ptr::null_mut(),
    };

    let post = Box::new(fie::data::Post {
        message: String::from(text),
        tags: Vec::new(),
        images: Vec::new(),
        flags: fie::data::PostFlags::default(),
    });

    Box::into_raw(post)
}

#[no_mangle]
///Sets NSFW flag to the post.
pub unsafe extern "C" fn fie_post_flag_set_snfw(post: *mut fie::data::Post, value: bool) {
    match post.as_mut() {
        Some(post) => post.flags.nsfw = value,
        _ => ()
    }
}

#[no_mangle]
///Appends hashtag to the Post
///
///Text must be encoded as UTF-8
///
///Returns whether operation is successful
pub unsafe extern "C" fn fie_post_add_tag(post: *mut fie::data::Post, text: *const c_char) -> bool {
    if text.is_null() {
        return false;
    }

    let post = match post.as_mut() {
        Some(post) => post,
        _ => return false,
    };

    let text = match ffi::CStr::from_ptr(text).to_str() {
        Ok(text) => text,
        Err(_) => return false,
    };

    post.tags.push(String::from(text));
    true
}

#[no_mangle]
///Adds image to Post
///
///Path must be encoded as UTF-8
///
///Returns whether operation is successful.
///Only up to 4 images are allowed
pub unsafe extern "C" fn fie_post_add_image(post: *mut fie::data::Post, path: *const c_char) -> bool {
    if path.is_null() {
        return false;
    }

    let post = match post.as_mut() {
        Some(post) => post,
        _ => return false,
    };

    if post.images.len() >= 4 {
        return false;
    }

    let path = match ffi::CStr::from_ptr(path).to_str() {
        Ok(path) => path,
        Err(_) => return false,
    };

    post.images.push(String::from(path));
    true
}

#[no_mangle]
///Frees memory for Post
pub unsafe extern "C" fn fie_post_free(ptr: *mut fie::data::Post) {
    crate::common::free_boxed(ptr);
}

#[repr(u8)]
///Result of send for API
pub enum ApiResult {
    ///API is not initialized so not sent
    NotSent = 0,
    ///Successfully sent
    Sent,
    ///Error sending
    Error,
}

impl Default for ApiResult {
    fn default() -> Self {
        ApiResult::NotSent
    }
}

impl Into<ApiResult> for Option<Result<fie::data::PostId, fie::api::ApiError>> {
    fn into(self) -> ApiResult {
        match self {
            Some(Ok(_)) => ApiResult::Sent,
            Some(Err(_)) => ApiResult::Error,
            None => ApiResult::NotSent
        }
    }
}

#[derive(Default)]
#[repr(C)]
///Result of posting.
pub struct PostResult {
    ///Whether was able to send post
    pub result: bool,
    ///Result of twitter post
    pub twitter: ApiResult,
    ///Result of gab post
    pub gab: ApiResult,
    ///Result of mastodon post
    pub mastodon: ApiResult,
    ///Result of minds post
    pub minds: ApiResult,
}

#[repr(C)]
///Specifies APIs to use
pub struct UsePlatforms {
    ///Use twitter.
    pub twitter: bool,
    ///Use gab.
    pub gab: bool,
    ///Use mastodon.
    pub mastodon: bool,
    ///Use minds.
    pub minds: bool,
}

#[no_mangle]
///Sends Post through API
///
///Consumes `post`
///
///If sent, returns result. On any failure returns NULL
pub unsafe extern "C" fn fie_api_send_post(api: *mut fie::API, post: *mut fie::data::Post) -> PostResult {
    let mut result = PostResult::default();

    if post.is_null() {
        return result;
    }

    let api = match api.as_mut() {
        Some(api) => api,
        _ => return result,
    };

    let post = Box::from_raw(post);
    let (twitter, gab, mastodon, minds) = match api.send(*post).wait() {
        Ok(post) => post.into_parts(),
        Err(_) => return result,
    };

    result.twitter = twitter.into();
    result.gab = gab.into();
    result.mastodon = mastodon.into();
    result.minds = minds.into();

    result
}

#[no_mangle]
///Sends Post through API
///
///Consumes `post`
///
///If sent, returns result.
///On any failure returns NULL
///
///`platforms` is optional argument
///If specified it determines toward which APIs to send post.
pub unsafe extern "C" fn fie_api_send_post_ext(api: *mut fie::API, post: *mut fie::data::Post, platforms: Option<&UsePlatforms>) -> PostResult {
    let mut result = PostResult::default();

    if post.is_null() {
        return result;
    }

    let api = match api.as_mut() {
        Some(api) => api,
        _ => return result,
    };
    //Necessary work-around for `AutoRuntime::wait` static bound limitation
    let api2 = &mut *(api as *mut fie::API);

    let (twitter, gab, mastodon, minds) = match platforms {
        Some(flags) => {
            let twitter = match flags.twitter {
                true => None,
                false => api.disable_twitter(),
            };
            let gab = match flags.gab {
                true => None,
                false => api.disable_gab(),
            };
            let mastodon = match flags.mastodon {
                true => None,
                false => api.disable_mastodon(),
            };
            let minds = match flags.minds {
                true => None,
                false => api.disable_minds(),
            };

            (twitter, gab, mastodon, minds)
        },
        None => (None, None, None, None)
    };


    let post = Box::from_raw(post);
    let send_result = match api.send(*post).wait() {
        Ok(post) => post.into_parts(),
        Err(_) => return result,
    };

    if twitter.is_some() {
        api2.enable_twitter(twitter);
    }
    if gab.is_some() {
        api2.enable_gab(gab);
    }
    if mastodon.is_some() {
        api2.enable_mastodon(mastodon);
    }
    if minds.is_some() {
        api2.enable_minds(minds);
    }

    let (twitter, gab, mastodon, minds) = send_result;
    result.twitter = twitter.into();
    result.gab = gab.into();
    result.mastodon = mastodon.into();
    result.minds = minds.into();

    result
}
