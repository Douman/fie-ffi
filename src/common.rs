//!Common FFI utilities

use core::ptr;

#[inline]
///Clones T pointer
pub unsafe fn clone_boxed<T: Clone>(ptr: *mut T) -> *mut T {
    if ptr.is_null() {
        return ptr::null_mut();
    }

    Box::into_raw(Box::from_raw(ptr).clone())
}

#[inline]
///Frees memory for T
pub unsafe fn free_boxed<T>(ptr: *mut T) {
    if ptr.is_null() {
        return;
    }

    drop(Box::from_raw(ptr));
}
