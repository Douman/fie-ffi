pub use tokio_global::{Runtime, AutoRuntime};
use tokio_global::tokio::runtime::Builder;

use std::io;

pub fn init() -> io::Result<Runtime> {
    let runtime = Builder::new().thread_name("fie-ffi")
                                .enable_io()
                                .basic_scheduler()
                                .build()?;

    Ok(Runtime::from_rt(runtime))
}
