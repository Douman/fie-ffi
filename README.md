# fie-ffi

[![Build](https://gitlab.com/Douman/fie-ffi/badges/master/pipeline.svg)](https://gitlab.com/Douman/fie-ffi/pipelines)
[![Crates.io](https://img.shields.io/crates/v/fie-ffi.svg)](https://crates.io/crates/fie-ffi)
[![Documentation](https://docs.rs/fie-ffi/badge.svg)](https://docs.rs/crate/fie-ffi/)
[![dependency status](https://deps.rs/crate/fie-ffi/0.3.0/status.svg)](https://deps.rs/crate/fie-ffi)

FFI bindings for my cute [Fie](https://github.com/DoumanAsh/fie)
